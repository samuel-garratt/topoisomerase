module Topoisomerase
  # Class to set up comments that will be added to stubs for dynamic methods
  class Comments
    class << self
      # @return [Object]  Class for which stubs will be created from and for
      #   which rules to add comments for are defined
      attr_accessor :inheriting_class

      # Add a comment that is returned if ALL matchers are true
      # @example Match a method ending with '_element' and include it's name without the '_element'
      #   comment "@return [Object] The '<%= method_to_stub.to_s.gsub('_element', '') %>' element",
      #           method_name: /_element$/
      # @param [String] comment Comment to add above class
      def comment(comment, matchers)
        Topoisomerase.comments_added[inheriting_class] << {
          message: comment,
          matchers: matchers
        }
      end

      # Add comments according to matchers for an inheriting class
      #
      # The order in which they are added is the order in which matching will occur.
      # The first comment that has a match will be used.
      # If several matchers are used on 1 comment, all must be true to be considered a match
      #
      # @example Add a comment where method name ends with '=' and source contains '.text'
      #   Comments.add_for_class PageObject do
      #     comment 'Comment to add', method_name: /=$/, source: /.text/
      #   end
      #
      # @param [Object] inheriting_class Class for which stubs will be created from and for
      #   which rules to add comments for are defined
      def add_for_class(inheriting_class, &script)
        Topoisomerase.comments_added[inheriting_class] ||= []
        self.inheriting_class = inheriting_class
        instance_eval(&script)
      end
    end
  end
end
