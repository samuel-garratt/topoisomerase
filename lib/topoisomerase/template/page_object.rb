# Load comments for page_object and create stubs for classes inheriting from it
require 'topoisomerase'

# This blocks shows the manual addition of comments to the comments_added accessor
# Preferred way is to Topoisomerase::Comments.add_for_class
#
# Topoisomerase.comments_added[PageObject] = [
#   {
#     message: "@return [PageObject::Elements::Element] An object representing the '<%= stub_method.gsub('_element', '') %>' element",
#     matchers: { method_name: /_element$/ } # If either matcher matches, result returned
#   },
#   {
#     message: "@return [Boolean] Whether the '<%= stub_method.gsub('?', '') %>' element is present",
#     matchers: { method_name: /\?$/ }
#   },
#   # Testing whether 2 matchers use AND
#   {
#     message: "Set the text of the '<%= stub_method.gsub('?', '') %>' text field\n # @param [String] _value Value to set",
#     matchers: { method_name: /=$/, source: /text_field_value_set/ }
#   },
#   {
#     message: "Set the value of '<%= stub_method.gsub('?', '') %>' element\n # @param [String] _value Value to set",
#     matchers: { method_name: /=$/ }
#   },
#   {
#     message: "@return [String] The text of the '<%= stub_method.gsub('?', '') %>' element",
#     matchers: { source: /\.text/ }
#   },
#   {
#     message: "Click the '<%= stub_method.gsub('?', '') %>' element",
#     matchers: { source: /\.click/ }
#   }
# ]

Topoisomerase::Comments.add_for_class PageObject do
  comment "Url of the page. Used by 'goto' method and when using 'visit'", method_name: 'page_url_value'
  comment 'Open the page on the browser', method_name: 'goto'
  comment "@return [PageObject::Elements::Element] An object representing the '<%= stub_method.gsub('_element', '') %>' element",
          method_name: /_element$/
  comment "Set the text of the '<%= stub_method.gsub('=', '') %>' text field\n # @param [String] _value Value to set",
          method_name: /=$/, source: /text_field_value_set/
  comment "Set the value of '<%= stub_method.gsub('=', '') %>' element\n # @param [String] _value Value to set",
          method_name: /=$/
  comment "Check the '<%= stub_method.gsub('check', '') %>' checkbox element", method_name: /^check_/
  comment "uncheck the '<%= stub_method.gsub('uncheck', '') %>' checkbox element", method_name: /^uncheck_/
  comment "@returns [Boolean] Whether the '<%= stub_method.gsub('_checked?', '') %>' Checkbox is checked",
          method_name: /_checked\?$/
  comment "Selects the '<%= stub_method.gsub('select', '') %>' radio element", method_name: /^select_/
  comment "Clears the selected '<%= stub_method.gsub('clear_', '') %>' radio element", method_name: /^clear_/
  comment "@returns [Boolean] Whether the '<%= stub_method.gsub('_selected?', '') %>' radiobox is selected",
          method_name: /_selected\?$/
  comment "@return [Boolean] Whether the '<%= stub_method.gsub('?', '') %>' element is present",
          method_name: /\?$/

end

# This block demonstrates how a real browser could be used to create stubs.
# This however should not be necessary and be resorted to only if the MockBrowser does not
# work
#
# @return [Array] List of Selenium options
# def options
#   [args: %w[--disable-popup-blocking
#             --no-sandbox --disable-dev-shm-usage],
#    headless: true]
# end
#
# require 'watir'
# browser = Watir::Browser.new :chrome, *options
# browser.close

# Mock class just to make instantiating a page object with a browser happy
class MockBrowser
  # Called by PageObject though not needed for documentation
  def bridge; end
end

Topoisomerase.create_stubs_based_on PageObject do |inheriting_class|
  inheriting_class.new(MockBrowser.new, false)
end
