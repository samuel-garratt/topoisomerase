lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'topoisomerase/version'

Gem::Specification.new do |spec|
  spec.name          = 'topoisomerase'
  spec.version       = Topoisomerase::VERSION
  spec.authors       = ['Samuel Garratt']
  spec.email         = ['samuel.garratt@integrationqa.com']

  spec.summary       = 'Used to create stubs for documenting dynamic Ruby methods.'
  spec.description   = 'Used to create stubs for documenting dynamic Ruby methods.'
  spec.homepage      = 'https://gitlab.com/samuel-garratt/topoisomerase'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://gitlab.com/samuel-garratt/topoisomerase.'
    spec.metadata['changelog_uri'] = 'https://gitlab.com/samuel-garratt/topoisomerase/CHANGELOG.md.'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'method_source'
  spec.add_dependency 'rubocop' # Clean up generated files
  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'page-object'
  spec.add_development_dependency 'ffi'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'webdrivers' # Only used for testing page-object gem
  spec.add_development_dependency 'yard'
end
