# Topoisomerase

This gem aims to creates stubs for dynamic methods allowing static documentation tools (e.g YARD) and IDEs to 
parse the files and create linkages for these methods.

## Inspiration

From wikipedia:

Topoisomerases are enzymes that participate in the overwinding or underwinding of DNA. 
The winding problem of DNA arises due to the intertwined nature of its double-helical structure. 
During DNA replication and transcription, DNA becomes overwound ahead of a replication fork.

The hope for this gem is to help document Ruby and `unwind` gems, libraries for those learning to use them. 

The gem `page_object_stub` also inspired me of what could be done. I wanted to take things a step further to 
make a more generic library. 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'topoisomerase'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install topoisomerase

## Usage

### For one class

Before creating stubs, make sure that you require all the files that you want to create stubs for.
Otherwise it will not create stubs for anything.

To create a stub file for a class, call the `create_stubs_for` passing that class.

E.g:
```ruby
Topoisomerase.create_stubs_for BasicClass 
```

By default stubs will be created in the `stubs` folder. 
This can be changed by setting the `stub_folder` variable. 

```ruby
Topoisomerase.stub_folder = 'custom_folder'
```

Note that this will by default attempt to instantiate the class with ClassName.new. If parameters are
needed pass a block to the `create_stubs_for` as shown below. The result of the block will be the 
instance of the class used in creating stubs.

```ruby
Topoisomerase.create_stubs_for ParameterizedClass do
  ParameterizedClass.new 5
end
```

### For classes inheriting from another

To create stubs for all classes inheriting from a class or module use the `create_stubs_based_on` method.
If the instantiation of such objects require parameters pass a parameter in the block to represent each class
that is found inheriting from the base class. 

Eg:

```ruby
Topoisomerase.create_stubs_based_on PageObject do |inheriting_class|
  inheriting_class.new(:config_1, 'config 2')
end
```

### Comments

If comments are used above a `define_method` method then they will be used as comments above the stub generated from it 
by default.

Otherwise the `Topoisomerase::Comments` class can be used. The following shows hows to add comments when generating stubs
for all classes inheriting from / including the `PageObject` module. It adds 

Eg:

```ruby
# 
Topoisomerase::Comments.add_for_class PageObject do
  comment "Set the '<%= method_to_stub.to_s.gsub('?', '') %>' text field", 
  method_name: /=$/, source: /text_field_value_set/
end
```

### Notes

For efficiency and maintainability, it's recommended that the least needed to instantiate the class that will still
generate the methods you need be used. However it may be just as easy to reuse the code you use when creating the objects
themselves if it is generic enough.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/samuel-garratt/topoisomerase. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Topoisomerase project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/samuel-garratt/topoisomerase/blob/master/CODE_OF_CONDUCT.md).
