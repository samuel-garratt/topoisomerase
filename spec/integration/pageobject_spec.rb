require 'pages/home_page'
require 'pages/login_page'
require 'pages/component/alert'
require 'pages/component/inner_defined_page'

RSpec.describe PageObject do
  let(:folder) { 'stub/page_object' }
  it 'handles class manually' do
    Topoisomerase.create_stubs_for LoginPage do
      LoginPage.new(MockBrowser.new, false)
    end
    gen = File.read File.join('stub', 'login_page.rb')
    expect(gen).to include 'def username_element'
  end
  it 'creates based on inheriting class' do
    Topoisomerase.create_stubs_based_on PageObject do |inheriting_class|
      inheriting_class.new(MockBrowser.new, false)
    end
    gen = File.read File.join(folder, 'home_page.rb')
    expect(gen).to include 'def heading_element'
    expect(gen).to include 'def heading?'
  end
end

RSpec.describe '.comments_added for PageObject' do
  let(:folder) { 'stub/page_object' }
  let(:page_object_array) do
    [
      {
        message: "@return [Watir::Elements::Element] An object representing the '<%= stub_method.gsub('_element', '') %>' element",
        matchers: { method_name: /_element$/ } # If either matcher matches, result returned
      },
      {
        message: "@return [Boolean] Whether the '<%= stub_method.gsub('?', '') %>' element is present",
        matchers: { method_name: /\?$/ }
      },
      # Testing whether 2 matchers use AND
      {
        message: "Set the text of the '<%= stub_method.gsub('=', '') %>' text field\n # @param [String] _value Value to set",
        matchers: { method_name: /=$/, source: /text_field_value_set/ }
      },
      {
        message: "Set the value of '<%= stub_method.gsub('=', '') %>' element\n # @param [String] _value Value to set",
        matchers: { method_name: /=$/ }
      },
      {
        message: "@return [String] The text of the '<%= stub_method %>' element",
        matchers: { source: /\.text/ }
      },
      {
        message: "Click the '<%= stub_method %>' element",
        matchers: { source: /\.click/ }
      }
    ]
  end
  let(:setup_comments) do
    Topoisomerase.comments_added[PageObject] = page_object_array
  end
  let(:create_page_stubs) do
    Topoisomerase.create_stubs_based_on PageObject do |inheriting_class|
      inheriting_class.new(MockBrowser.new, false)
    end
  end
  let(:login_page) { File.read File.join(folder, 'login_page.rb') }
  let(:home_page) { File.read File.join(folder, 'home_page.rb') }
  before do
    FileUtils.rm_rf Topoisomerase.stub_folder
    Topoisomerase.comments_added = {}
  end
  it 'adds comments through hash' do
    setup_comments
    create_page_stubs
    expect(login_page).to include "An object representing the 'login' element\n  def login_element"
  end
  it 'ensures both matchers are met' do
    # TODO: Do this through matcher class
    setup_comments
    create_page_stubs
    expect(home_page).to include "# Set the value of 'custom_setter' element
  # @param [String] _value Value to set\n  def custom_setter="
  end
  it 'adds comments through class' do
    Topoisomerase::Comments.add_for_class PageObject do
      comment 'Test Comment', method_name: /=$/, source: /text_field_value_set/
    end
    create_page_stubs
    expect(login_page).to include "# Test Comment\n  def username="
  end

end
