# Class that needs parameters set to be called correctly
class ParameterizedClass
  attr_accessor :number

  # @param [Integer] number This must be passed
  def initialize(number)
    self.number = number
  end

  2.times do |num|
    # Comment for dynamic
    define_method "dynamic_#{num}" do
      puts 'dynamic'
    end
  end

  def non_dynamic_method
    'test'
  end
end
