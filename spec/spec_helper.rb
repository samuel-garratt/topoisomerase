require 'bundler/setup'
require 'topoisomerase'
require 'webdrivers' # Used for testing of PageObject instantiation

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

# Mock class just to make instantiating a page object with a browser happy
class MockBrowser
  # Called by PageObject though not needed for documentation
  def bridge; end
end
