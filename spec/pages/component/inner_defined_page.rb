module Inner
  class DefinedPage
    include PageObject

    button :inner_but, id: 'inner'

    # Example of using inner definitions
    def locate_inner
      inner_but if inner_but_element.visible?
    end
  end
end
