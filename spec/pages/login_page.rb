require 'page-object'
class LoginPage
  include PageObject

  text_field :username, id: 'username'
  button :login, id: 'login'
  checkbox :confirm_application, id: 'check'
  radio_button :radio_button, id: 'radio'

  # Login with user
  # Using Rubymine methods here are auto-completed according to stub
  # @param [String] user
  def login_with(user)
    self.username = user if username?
    username_element.flash
    raise 'Cannot see Login button' unless login?

    login
  end
end
