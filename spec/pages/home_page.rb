require 'page-object'
class HomePage
  include PageObject

  page_url 'https://test_url'

  div :heading, id: 'heading'

  define_method('custom_setter=') do |val1, val2|
    puts val1 + val2
  end
end
