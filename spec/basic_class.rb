# Class that can be instantiated with simply Class.new
class BasicClass
  # Test Comment
  define_method 'dynamic_in_class' do
    puts 'This should also be ignored'
  end

  define_method 'method_with_params' do |param1, param2|
    puts "Param #{param1} and #{param2}"
  end

  def static_def_method
    puts 'hi'
  end
end
