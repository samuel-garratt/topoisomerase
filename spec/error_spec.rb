require 'basic_class'

RSpec.describe 'Errors' do
  it 'thrown when calling method on generated class' do
    Topoisomerase.create_stubs_for BasicClass
    require_relative '../stub/basic_class'
    b = BasicClass.new
    expect { b.dynamic_in_class }.to raise_error Topoisomerase::Error, /dynamic_in_class/
  end
end