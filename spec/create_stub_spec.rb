require 'basic_class'
require 'parameterized_class'

RSpec.describe '.create_stubs_for' do
  it 'stubs for BasicClass' do
    Topoisomerase.create_stubs_for BasicClass
    gen = File.read 'stub/basic_class.rb'
    expect(gen).to include 'def dynamic_in_class'
  end
  it 'stubs for ParameterizedClass' do
    Topoisomerase.create_stubs_for ParameterizedClass do
      ParameterizedClass.new 5
    end
    gen = File.read 'stub/parameterized_class.rb'
    expect(gen).to include 'def dynamic_0'
  end
end