require 'basic_class'
require 'parameterized_class'

RSpec.describe BasicClass do
  subject(:methods) { Topoisomerase.dynamic_instance_methods(described_class) }
  it 'can find dynamic methods' do
    expect(methods.keys.count).to eq 2
    expect(methods[:dynamic_in_class][:comment]).to eq "# Test Comment\n"
  end
  it 'carrier across method params' do
    expect(methods[:method_with_params][:parameters]).to eq %w[param1 param2]
  end
end

RSpec.describe ParameterizedClass do
  let(:methods) do
    Topoisomerase.dynamic_instance_methods ParameterizedClass do
      ParameterizedClass.new 5
    end
  end
  it 'can find dynamic methods' do
    expect(methods.keys.count).to eq 2
    expect(methods.keys).to match_array %i[dynamic_0 dynamic_1]
  end
  it 'shows source' do
    expect(methods[:dynamic_0][:source]).to include 'define_method'
  end
  it 'shows comment' do
    expect(methods[:dynamic_0][:comment]).to include 'Comment for dynamic'
  end
  it 'has correct location' do
    expect(methods[:dynamic_0][:location][0]).to end_with 'parameterized_class.rb'
  end
  it 'has line number' do
    expect(methods[:dynamic_0][:location][1]).to eq 12
  end
end
